/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lzwbintree;
import java.util.*;
import java.lang.*;
/**
 *
 * @author Dr.Chase
 */
public abstract class BinTree<E> {
    protected Node<E> root;
    
    public Node<E> travellingNode;
    
    //3 variables which we will need
    protected int debthOfTree;
    protected double averageLeafDepth;
    protected double deviation; // formula: http://www.inf.unideb.hu/valseg/JEGYZET/megj/meg11.htm 
                                // http://progpater.blog.hu/2011/03/05/labormeres_otthon_avagy_hogyan_dolgozok_fel_egy_pedat
    
    public ArrayList< Node<E> > leaves = new ArrayList<>();
    
    public BinTree()    {
        this.root = null;
        travellingNode = null;
    }
    public BinTree(E element)    {
        this.root = new Node(element);
        travellingNode = this.root;
    }
    
    public boolean isEmpty()    {
        if( this.root == null)
            return true;
        else
            return false;
    }
    
    // getters
    public Node<E> getRoot()    {
        return this.root;
    }
    
    // setters
    public void setDebthOfTree(int input)   {
        debthOfTree = input;
    }
    public void traverseTreeHelper(Node<E> inputNode)  {   //and get leafs
//        if (root == null)   {
//            setDebthOfTree(0);
//            return;
//        }
//        else    {
        Node<E> localTravellingNode = inputNode;
            //debugging
//            System.out.println(localTravellingNode.toString());
            
            
            if(localTravellingNode.getLeftChild() != null)   {
                //debugging
//                System.out.println("going left");
                traverseTreeHelper(localTravellingNode.getLeftChild());
            }
            if(localTravellingNode.isLeaf())    {
                //debugging
//                System.out.println("adding leaf");
                leaves.add(localTravellingNode);
            }
            
            if(localTravellingNode.getRightChild() != null)   {
                //debugging
//                System.out.println("going right");
                traverseTreeHelper(localTravellingNode.getRightChild());
            }
//        }  
    }
    public int getDepth()   {
        int nMax = 0;
        for(int nIndex = 0; nIndex < leaves.size(); nIndex++)    {
            if(this.debthOfTree < leaves.get(nIndex).nodeDepth )   
                this.debthOfTree = leaves.get(nIndex).nodeDepth;
        }
        return this.debthOfTree;
    }
    public double getAverageLeafDepth ()   {
        int sum = 0;
        for(int nIndex = 0; nIndex < leaves.size(); nIndex++)   {
            sum += leaves.get(nIndex).nodeDepth;
        }
        averageLeafDepth = ((double)sum) / (double)leaves.size();
        return averageLeafDepth;
    }
    
    public double getDeviation()    {
        double sum = 0.0;
        for(int nIndex = 0; nIndex < leaves.size(); nIndex++)   {
            sum += ((leaves.get(nIndex).nodeDepth - averageLeafDepth) * (leaves.get(nIndex).nodeDepth - averageLeafDepth));
        }
        deviation = Math.sqrt( sum / (leaves.size() - 1));
        return deviation;
    }
}
