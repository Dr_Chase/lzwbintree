/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lzwbintree;
import java.io.*;
/**
 *
 * @author Dr.Chase
 */
public class LZWBinTree {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        //Tests
//        Node<Integer> test = new Node<>(4);
//        test.leftChild = new Node<Integer>(2);
//        System.out.println(test.getElement());
//        System.out.println(test.hasLeftChild());
//        
//        Node<Integer> cloned = new Node<Integer>();
//        System.out.println(test.getElement());
//        System.out.println(cloned.getElement());

//        LZWTreeClass myTree = new LZWTreeClass();
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        //System.out.println(myTree.getRoot().getElement());
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)0);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
//        myTree.addByte((byte)1);
        
//        //System.out.println(myTree.travellingNode.getElement());
//        

        
        //System.out.println("fasz");
        try {
            LZWTreeClass BinaryTree = new LZWTreeClass();
            String fileName = args[0];
            
            byte[] buffer = new byte[1];
            
            FileInputStream inputStream = new FileInputStream(fileName);
            //System.out.println(args[0]);
            
            int nRead = 0;
            // A = 65, C = 67, G = 84, T = 71
            boolean bInComment = false;
            while( (nRead = inputStream.read(buffer)) != -1 ) {
                if (buffer[0] == 0x3e) {
                    bInComment = true;
                    continue;
                }

                if (buffer[0] == 0x0a) {
                    bInComment = false;
                    continue;
                }

                if (bInComment) {
                    continue;
                }

                if (buffer[0] == 0x4e)
                {
                   continue;
                }
                if( buffer[0] == (byte)65 || buffer[0] == (byte)67 || buffer[0] ==(byte)84 || buffer[0] ==(byte)71  )   {
                   //System.out.println(buffer[0]);
                   byte actualByte = buffer[0];
                    for (int i = 0; i < 8; i++) {
                        if( (byte) ( (byte)actualByte & (byte) 0x80) == (byte) 0x80 )    {
                            BinaryTree.addByte((byte) 1 );
                        }
                        else
                            BinaryTree.addByte((byte) 0 );
                        actualByte = (byte)(actualByte << (byte) 1);
                    }
                }
                
                else
                    continue;
                
            }
            inputStream.close();
                            // ================================================================
            BinaryTree.traverseTreeHelper(BinaryTree.getRoot());
            System.out.println("======================================================");
            //System.out.println(BinaryTree.leaves.size());
            System.out.println("Depth: \t\t" + BinaryTree.getDepth());
            System.out.println("AvgLeafDepth: \t" + BinaryTree.getAverageLeafDepth());
            System.out.println("Deviation: \t" + BinaryTree.getDeviation());
            System.out.println("======================================================");
            
        }
        catch(FileNotFoundException ex) {
//            System.out.println(
//                "Unable to open file '" + 
//                fileName + "'"
                  ex.printStackTrace();
//            );                
        }
        catch(IOException ex) {
//            System.out.println(
//                "Error reading file '" 
//                + fileName + "'");                  
//             Or we could just do this: 
             ex.printStackTrace();
            
        }
        catch (Exception e) {
            System.out.println("Unknown Error");
        }

        
    }
    
}
