/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lzwbintree;

import java.lang.reflect.*;

/**
 *
 * @author Dr.Chase
 */
public class Node<E> {
    protected E data;
    protected Node<E> leftChild;
    protected Node<E> rightChild;
    protected Node<E> parent;
    
    protected int nodeDepth;
    
    //constructors
    public Node(E input)    {
        this();
        data = input;
        leftChild = null;
        rightChild = null;
        parent = null;
    }
    public Node ()  {
       data = (E) new Object();
       leftChild = null;
       rightChild = null;
       parent = null;
    }
//    public Node (Class<E> genericFactory) {
//        this.data = genericFactory.newInstance();
//    }
//    public Node (Node<E> input) {
//        this.data = input.clone();
//    }
    
    //Deep Copy -- doesn't work... --
    @SuppressWarnings("unchecked")
    @Override
    public Object clone()   {
        Node<E> cloned = new Node<E>();
        try {
            cloned.data = (E) data.getClass().getMethod("clone").invoke(data);
        } catch (Exception e)   {
            System.out.println("cloning failed...");
        }
        return cloned;
    }
    
    // yes--no questions
    public boolean hasLeftChild()  {
        if(leftChild != null)
            return true;
        else 
            return false;
    };
    public boolean hasRightChild()  {
        if(rightChild != null)
            return true;
        else 
            return false;
    };
    public boolean hasParent()  {
        if(parent != null)
            return true;
        else 
            return false;
    };
    
    // Getters
    public Node<E> getLeftChild()   {
        return leftChild;
    }
    
    public Node<E> getRightChild()   {
        return rightChild;
    }
    
    public Node<E> getParent()   {
        return parent;
    }
    
    public int getNodeDepth()   {
        return this.nodeDepth;
    }
    
    protected E getElement()  {
        return data;
    }
    
    
    // Setters
    // Setting leftChild
    public void setLeftChild(Node<E> input)  {
        this.leftChild = input;
    }
    public void setLeftChild(E input)  {
        this.leftChild = new Node<E>(input);
    }
    
    // Setting Right Child
    public void setRightChild(Node<E> input)  {
        this.rightChild = input;
    }
    public void setRightChild(E input)  {
        this.rightChild = new Node<E>(input);
    }
    
    // Set the parent
    public void setParent(Node<E> input)  {
        this.parent = input;
    }
//    public void setParent(E input)  {
//        this.parent = new Node<E>(input);
//    }
    
    // Set nodeDebth
    public void setNodeDepth(int input)  {
        this.nodeDepth = input;
    }
    
    // getting information about nodes
    public boolean isLeaf() {
        if( !hasLeftChild() && !hasRightChild() )    {
            return true;
        }
        else
            return false;
    }
    public boolean isMiddle() {
        if( hasLeftChild() || hasRightChild())    {
            return true;
        }
        else
            return false;
    }
    @Override
    public String toString()    {
        String output = "---------------\n" +
                        "data: " + this.data.toString() +    "\n" +
                        "hasLeftChild: " + hasLeftChild() +  "\n" +
                        "hasRightChild: "+ hasRightChild() + "\n" +
                        "hasParent: " + hasParent() +        "\n" +
                        "depth: " + getNodeDepth() +         "\n" +
                        "---------------\n"
        ;
        return output;
        
                
    }
    
    
    
}
