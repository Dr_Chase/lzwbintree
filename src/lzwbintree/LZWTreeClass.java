/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lzwbintree;

import java.lang.*;
import java.util.*;
/**
 *
 * @author Dr.Chase
 */
public class LZWTreeClass extends BinTree<Byte> {
    public LZWTreeClass()   {
        super(new Byte((byte)'/'));
    }
    public StringBuilder szamok = new StringBuilder("eddigi: ");
    public void addByte(Byte input)  {
        if(this.travellingNode == null && this.root == null) {
            this.root = new Node<Byte>(input);
            this.root.setNodeDepth(0);
            this.travellingNode = this.root;
        }

        else if(this.root != null)    {
            if( input == (byte) 0 )    {
                if(!travellingNode.hasLeftChild()){
                    this.travellingNode.setLeftChild((new Byte("0")));
                    this.travellingNode.getLeftChild().setParent(this.travellingNode);
                    this.travellingNode.getLeftChild().setNodeDepth(this.travellingNode.getNodeDepth() + 1);
                    this.travellingNode = this.travellingNode.getLeftChild();
                    
                    //debugging
//                    System.out.println("writing 0 and back to root" );
//                    szamok.append("0");
//                    System.out.println(szamok.toString() + "\n" + travellingNode.toString() );
                    
                    this.travellingNode = this.getRoot();
                }
                else    {
                    this.travellingNode = this.travellingNode.getLeftChild();
                    //debugging
//                    szamok.append("0");
//                    System.out.println("moved left 0\n" + szamok.toString()+ "\n" + travellingNode.toString());
                }
                    
            }
            else if (input == (byte) 1 )   {
                if(!travellingNode.hasRightChild()){
                    this.travellingNode.setRightChild((new Byte("1")));
                    this.travellingNode.getRightChild().setParent(this.travellingNode);
                    this.travellingNode.getRightChild().setNodeDepth(this.travellingNode.getNodeDepth() + 1);
                    this.travellingNode = this.travellingNode.getRightChild();
                    
                    //debugging
//                    System.out.println("writing 1 and back to root" );
//                    szamok.append("1");
//                    System.out.println(szamok.toString() + "\n" + travellingNode.toString() );
                    
                    this.travellingNode = this.getRoot();
                }
                else    {
                    this.travellingNode = this.travellingNode.getRightChild();
                    //debugging
//                    szamok.append("1");
//                    System.out.println("moved right 1\n" + szamok.toString()+ "\n" + travellingNode.toString());
                }
            }
        }
        else    {
            // Throw some custom exception TODO
        }
    }
}
