/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lzwbintree;
import java.io.*;

/**
 *
 * @author Dr.Chase
 */
// throws FileNotFoundException
public class BinTreeFileIO  {
    LZWTreeClass BinaryTree;
    String fileName; 
    File file;
    public BinTreeFileIO(String inputFile)  throws FileNotFoundException, IOException {
        this.fileName = inputFile;
        BinaryTree = new LZWTreeClass();
        BuildTree(this.fileName);
    }
    
    public BinTreeFileIO(File inputFile)  throws FileNotFoundException, IOException {
        this.file = inputFile;
        BinaryTree = new LZWTreeClass();
        BuildTree(inputFile);
    }
    
    
    private void BuildTree(String inputFile) throws FileNotFoundException, IOException   {
        byte[] buffer = new byte[1];

        FileInputStream inputStream = new FileInputStream(inputFile);
        //System.out.println(args[0]);

        int nRead = 0;
        // A = 65, C = 67, G = 84, T = 71
        boolean bInComment = false;
        while( (nRead = inputStream.read(buffer)) != -1 ) {
            if (buffer[0] == 0x3e) {
                bInComment = true;
                continue;
            }
            if (buffer[0] == 0x0a) {
                bInComment = false;
                continue;
            }
            if (bInComment) {
                continue;
            }
            if (buffer[0] == 0x4e)
            {
               continue;
            }
            if( buffer[0] == (byte)65 || buffer[0] == (byte)67 || buffer[0] ==(byte)84 || buffer[0] ==(byte)71  )   {
               //System.out.println(buffer[0]);
               byte actualByte = buffer[0];
                for (int i = 0; i < 8; i++) {
                    if( (byte) ( (byte)actualByte & (byte) 0x80) == (byte) 0x80 )    {
                        BinaryTree.addByte((byte) 1 );
                    }
                    else
                        BinaryTree.addByte((byte) 0 );
                    actualByte = (byte)(actualByte << (byte) 1);
                }
            }
            else
                continue;
        }
        inputStream.close();
        BinaryTree.traverseTreeHelper(BinaryTree.getRoot());
    }
    
        private void BuildTree(File inputFile) throws FileNotFoundException, IOException   {
        byte[] buffer = new byte[1];

        FileInputStream inputStream = new FileInputStream(inputFile);
        //System.out.println(args[0]);

        int nRead = 0;
        // A = 65, C = 67, G = 84, T = 71
        boolean bInComment = false;
        while( (nRead = inputStream.read(buffer)) != -1 ) {
            if (buffer[0] == 0x3e) {
                bInComment = true;
                continue;
            }
            if (buffer[0] == 0x0a) {
                bInComment = false;
                continue;
            }
            if (bInComment) {
                continue;
            }
            if (buffer[0] == 0x4e)
            {
               continue;
            }
            if( buffer[0] == (byte)65 || buffer[0] == (byte)67 || buffer[0] ==(byte)84 || buffer[0] ==(byte)71  )   {
               //System.out.println(buffer[0]);
               byte actualByte = buffer[0];
                for (int i = 0; i < 8; i++) {
                    if( (byte) ( (byte)actualByte & (byte) 0x80) == (byte) 0x80 )    {
                        BinaryTree.addByte((byte) 1 );
                    }
                    else
                        BinaryTree.addByte((byte) 0 );
                    actualByte = (byte)(actualByte << (byte) 1);
                }
            }
            else
                continue;
        }
        inputStream.close();
        BinaryTree.traverseTreeHelper(BinaryTree.getRoot());
    }
    
    @Override
    public String toString()    {
        StringBuilder sb = new StringBuilder();
        sb.append("======================================================"); sb.append(System.lineSeparator());
        sb.append("Depth: \t\t"); sb.append(BinaryTree.getDepth()); sb.append(System.lineSeparator());
        sb.append("AvgLeafDepth: \t"); sb.append(BinaryTree.getAverageLeafDepth()); sb.append(System.lineSeparator());
        sb.append("Deviation: \t"); sb.append(BinaryTree.getDeviation());sb.append(System.lineSeparator());
        sb.append("======================================================\n"); sb.append(System.lineSeparator());
        return sb.toString();
    }
    public String toHTMLString()    {
        StringBuilder sb = new StringBuilder();
        sb.append("<p>======================================================</p>"); //sb.append(System.lineSeparator()); sb.append("<br/>");
        sb.append("<p>Depth: \t\t"); sb.append(BinaryTree.getDepth()); sb.append("</p>"); //sb.append(System.lineSeparator()); sb.append("<br/>");
        sb.append("<p>AvgLeafDepth: \t"); sb.append(BinaryTree.getAverageLeafDepth()); sb.append("</p>");// sb.append(System.lineSeparator()); sb.append("<br/>");
        sb.append("<p>Deviation: \t"); sb.append(BinaryTree.getDeviation()); sb.append("</p>"); //sb.append(System.lineSeparator()); sb.append("<br/>");
        sb.append("<p>======================================================</p>"); //sb.append(System.lineSeparator()); sb.append("<br/>");
        return sb.toString();
    }
    
}
